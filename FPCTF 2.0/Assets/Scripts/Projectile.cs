using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject projectile;
    public Transform firePoint;
    public float projectileSpeed;
    private bool isPlayer;
    public float shootRate;
    private float lastShootTime;
    public int curAmmo;
    public int maxAmmo;
    public bool infiniteAmmo;

    void Awake()
    {
        if(GetComponent<PlayerController>())
            isPlayer = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public bool CanShoot()
    {
        if(Time.time - lastShootTime >= shootRate)
        {
            if(curAmmo > 0 || infiniteAmmo == true)
                return true;
        }
        return false;
    }

    public void Shoot()
    {
        lastShootTime = Time.time;
        curAmmo--;

        GameObject projectileObject = Instantiate(projectile, firePoint.position, firePoint.rotation);
        projectileObject.GetComponent<Rigidbody>().velocity = firePoint.forward * projectileSpeed;
        
    }

}
